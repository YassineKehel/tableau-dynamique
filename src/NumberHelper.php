<?php

namespace App;

class NumberHelper
{

    public static function price(int $number, string $sigle = "€"): string
    {
        return number_format($number, 0, '', ' ') . ' ' . $sigle;
    }
}
