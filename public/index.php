<?php

use App\NumberHelper;
use App\QueryBuilder;
use App\Table;
use App\TableHelper;
use App\UrlHelper;

define('PER_PAGE', 7);
require '../vendor/autoload.php';
require '../connect.php';
$connexion = "mysql:host=" . SERVER . ";port=3308; dbname=" . DB;
try {
    $pdo = new PDO($connexion, USER, PASSWORD, [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    ]);
} catch (PDOException $pe) {
    printf("Echec de la connexion : %s\n", $pe->getMessage());
    exit();
}
$query = (new QueryBuilder($pdo))->from('voitures');
// $sortable = ["id", "nom", "marque", "prix", "annee"];


// Recherche par nom
if (!empty($_GET['q'])) {
    $query
        ->where('nom LIKE :nom')
        ->setParam('nom', $_GET['q'] . '%');
}

$table = (new Table($query, $_GET))
    ->sortable('id', 'nom', 'marque')
    ->format('prix', function ($v) {
        return NumberHelper::price($v);
    })
    ->columns([
        'id' => 'ID',
        'nom' => 'Nom',
        'marque' => 'Marque',
        'prix' => 'Prix'
    ]);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Liste de voitures</title>
</head>

<body class="p-4">

    <h1>Les voitures</h1>
    <form action="" class="mb-4">
        <div class="form-group">
            <input type="text" class="form-control" name="q" placeholder="Rechercher par nom" value="<?= htmlentities($_GET['q'] ?? null) ?>">
        </div>
        <button class="btn btn-primary mt-3">Rechercher</button>
    </form>
    <?php $table->render() ?>

</body>

</html>